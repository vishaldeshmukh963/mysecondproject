package com.student.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.student.model.Student;
import com.student.service.StudentService;

@RestController
public class StudentController {

	@Autowired
	private StudentService studentService;
	
	@PostMapping("/saveStudent")
	public Student saveStudent(@RequestBody Student student) {   
		Student saveStudent = studentService.saveStudent(student);
		return saveStudent;
	}
	
	@PostMapping("/saveStudentWithEmail")
	public Student saveStudentWithEmail(@RequestBody Student student) {
		Student saveStudentWithEmail = studentService.saveStudent(student);
		return saveStudentWithEmail;
	}
	
	@PutMapping("/updateStudentById/{id}")
	public Student updateStudentById(@PathVariable Long id,@RequestBody Student student) {
		Student updateStudentwithId = studentService.updateStudentwithId(id, student);
		return updateStudentwithId;
	}
	
	@PutMapping("/updateStudent")
	public Student updateStudent(@RequestBody Student student) {
		Student updateStudent = studentService.updateStudent(student);
		return updateStudent;
	}
	
	@DeleteMapping("/deleteStudentById/{id}")
	public void deleteStudentById(@PathVariable("id") Long idLong) {
		studentService.deleteStudent(idLong);
	}
	
	@GetMapping("/getListStudents")
	public List<Student> getListStudents() {
		List<Student> student = studentService.getStudent();
		return student;
	}
	
	@GetMapping("/getStudentByLastName/{lastName}")
	public List<Student> getStudentByLastName(@PathVariable String lastName) {
		List<Student> studentByLastName = studentService.studentByLastName(lastName);
		return studentByLastName;
	}
}
