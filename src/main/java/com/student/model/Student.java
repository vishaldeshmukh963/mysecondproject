package com.student.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "student_Details")
public class Student {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id; // pk
	
	@Column(name = "first_Name")
	private String firstName;
	
	@Column(name = "last_Name")
	private String lastName;

	@OneToOne(targetEntity = StudentEmail.class,cascade = CascadeType.ALL)
	private StudentEmail studentEmail;
	
	
	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Student(String firstName, String lastName, StudentEmail studentEmail) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.studentEmail = studentEmail;
	}


	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public StudentEmail getStudentEmail() {
		return studentEmail;
	}

	public void setStudentEmail(StudentEmail studentEmail) {
		this.studentEmail = studentEmail;
	}

	
	
	@Override
	public String toString() {
		return "Student [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", studentEmail="
				+ studentEmail + "]";
	}
	
}
