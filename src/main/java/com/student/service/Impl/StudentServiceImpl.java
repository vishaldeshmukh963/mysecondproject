package com.student.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.student.Repo.StudentRepository;
import com.student.model.Student;
import com.student.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	private StudentRepository studentRepository;

	@Override
	public Student saveStudent(Student student) {
		// TODO Auto-generated method stub
		Student saveStudent = studentRepository.save(student);
		return saveStudent;
	}

	@Override
	public Student updateStudent(Student student) {
		// TODO Auto-generated method stub
		Student updateStudent = studentRepository.save(student);
		return updateStudent;
	}

	@Override
	public void deleteStudent(Long idLong) {
		// TODO Auto-generated method stub
		studentRepository.deleteById(idLong);
    		 
	}

	@Override
	public List<Student> getStudent() {
		// TODO Auto-generated method stub
		List<Student> allStudents = studentRepository.findAll();
		return allStudents;
	}

	@Override
	public List<Student> studentByLastName(String lastName) {
		// TODO Auto-generated method stub
		List<Student> findByLastName = studentRepository.findByLastName(lastName);
		return findByLastName;
	}

	@Override
	public Student updateStudentwithId(Long id, Student student) {
		// TODO Auto-generated method stub
		Student student2 = studentRepository.findById(id).orElse(null);
		if (student2 !=null) {
			student2.setStudentEmail(student.getStudentEmail());
		}
		return studentRepository.save(student2);
	}
	
}
