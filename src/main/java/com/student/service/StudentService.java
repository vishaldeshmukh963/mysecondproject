package com.student.service;
import java.util.List;

import com.student.model.Student;


public interface StudentService {

	public Student saveStudent(Student student);
	
	public Student updateStudent(Student student);
	
	public void deleteStudent(Long idLong);
	
	public List<Student> getStudent();
	
	public List<Student> studentByLastName(String lastName);
	
	public Student  updateStudentwithId(Long id, Student student);
	
}
