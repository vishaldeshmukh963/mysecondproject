package com.student.Repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.student.model.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long>{

	@Query(value = "SELECT * FROM student_details WHERE last_name = ?1" ,nativeQuery = true)
	List<Student> findByLastName(String lastName);
	
}

